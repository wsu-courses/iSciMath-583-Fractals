import simpleaudio as sa
import numpy as np
import scipy as sp
import scipy.signal


class Waveforms:
    @staticmethod
    def saw(w, dx=0.03, **kw):
        """Return a saw wave."""
        dx = max(dx, 1e-12)
        xs = [0, 0.5-dx, 0.5+dx, 1]
        As = [0, 1, -1, 0]
        x = (w / 2 / np.pi) % 1
        return np.interp(x, xs, As)

    @staticmethod
    def triangle(w, **kw):
        """Return a trangle wave."""
        xs = [0, 0.25, 0.75, 1]
        As = [0, 1, -1, 0]
        x = (w / 2 / np.pi) % 1
        return np.interp(x, xs, As)


class Play:
    """Simple class to play sounds."""
    sample_rate = 44100
    volume = 0.1
    freq_440_Hz = 440.0   # Tuning
    sample_type = np.int16
    
    def __init__(self, **kw):
        for key in kw:
            if not hasattr(self, key):
                raise NotImplementedError(f"Unknown parameter {key}.")
            setattr(self, key, kw[key])
        self.init()

    def init(self):
        self.max = np.iinfo(self.sample_type).max

    def get_sound(self, f, T=2):
        """Return

        Arguments
        ---------
        f : function
            Return f(t), the waveform.
        T : float
            Duration of note in s.
        """
        samples = int(T * self.sample_rate)
        t = np.linspace(0, T, samples, False)
        audio = f(t)
        # normalize to 16-bit range
        audio *= self.max / np.max(np.abs(audio))
        audio *= self.volume
        # convert to 16-bit data
        audio = audio.astype(self.sample_type)
        return audio

    def play_sound(self, audio):
        # start playback
        play_obj = sa.play_buffer(audio, 1, 2, self.sample_rate)

        # wait for playback to finish before exiting
        play_obj.wait_done()
        return audio

    def f(self, t,
          freq_Hz=440.0,
          harmonics=np.arange(1,10),
          gamma=1.05,
          A=lambda n: (n%2)/n
         ):
        f_t = sum(A(_n)*np.sin((2*np.pi * freq_Hz * _n**gamma)*t)
                  for _n in harmonics)
        window = sp.signal.get_window('bartlett', len(t))
        return window * f_t

    def play_chord(self,
                   harmonics=np.arange(1,10),
                   waveform=np.sin,
                   T=2.0,
                   A=lambda n: (n%2)/n,
                   gamma=1.00):
        def f(t):
            window = sp.signal.get_window('bartlett', len(t))
            T = t[-1] - t[0]
            window = np.interp(t-t[0],
                               [0, 0.01, T-0.1, T],
                               [0, 1, 1.0, 0])
            res = 0
            for _n in harmonics:
                T = 1/(self.freq_440_Hz * _n**gamma)
                w = 2*np.pi * ((t / T) % 1)
                res = res + A(_n)*waveform(w)
            return window * res
        audio = self.get_sound(f=f, T=T)
        self.play_sound(audio)
