"""Plotting Tools."""

import numpy as np
from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation
from IPython.display import HTML, display


class CobWeb:
    """Class to draw and animate cobweb diagrams.

    This assumes a similar form to the logistic map `f(x, r) = r*x*(1-x)` with a single
    parameter.

    Parameters
    ----------
    f : function
        Function to iterate $x\mapsto f(x, r)$.  Here `x` is analogous to the
        population, while `r` is the population growth parameter.
    x0 : float
        Initial value `0 <= x0 <= 1`
    N0, N : int
        Skip the first `N0` iterations then plot `N` iterations.
    ax : Axis, None
        Axis to use for plotting.
    """

    def __init__(self, f, x0=0.2, N0=0, N=1000):
        self.f = f
        self.x0 = x0
        self.N0 = N0
        self.N = N
        self.ax = None
        self.fig = None
        self.artists = None

    def cobweb(self, r, ax=None):
        """Draw a cobweb diagram.

        Arguments
        ---------
        r : float
            Growth factor 0 <= r <= 4.

        Returns
        -------
        artists : list
            List of updated artists.
        """
        # Generate population
        x0 = self.x0
        xs = [x0]
        for n in range(self.N0 + self.N + 1):
            x0 = self.f(x0, r)
            xs.append(x0)

        xs = xs[self.N0 :]  # Skip N0 initial steps

        # Manipulate data for plotting
        Xs = np.empty((len(xs), 2))
        Ys = np.zeros((len(xs), 2))
        Xs[:, 0] = xs
        Xs[:, 1] = xs
        Ys[1:, 0] = xs[1:]
        Ys[:-1, 1] = xs[1:]
        Xs = Xs.ravel()[:-2]
        Ys = Ys.ravel()[:-2]

        if self.N0 > 0:
            Xs = Xs[1:]
            Ys = Ys[1:]

        x = np.linspace(0, 1, 200)
        y = self.f(x, r)
        title = f"$r={r:.2f}$"
        if ax is None:
            if self.ax is None:
                self.fig, self.ax = plt.subplots()
            ax = self.ax

        if self.artists is None:
            artists = self.artists = []
            artists.extend(ax.plot(Xs, Ys, "r", lw=0.5))
            artists.extend(ax.plot(x, y, "C0"))
            artists.extend(ax.plot(x, x, "k"))
            ax.set(xlim=(0, 1), ylim=(0, 1), title=title)
            artists.append(ax.title)
        else:
            artists = self.artists[:2] + self.artists[-1:]
            artists[0].set_data(Xs, Ys)
            artists[1].set_data(x, y)
            artists[2].set_text(title)
        return artists

    def init_func(self):
        return self.cobweb(r=1.0)

    def animate(self, rs=None, interval_ms=20):
        """Animate the cobweb plot over a range of parameters.

        Arguments
        ---------
        rs : iterable
            Parameters to iterate over.  If None, then we increase from 1 to 4 slowing
            as we approach 4 (customized for the logistic map).
        """
        if rs is None:
            # Slow down as we get close to 4.
            x = np.sin(np.linspace(0.0, 1.0, 100) * np.pi / 2)
            rs = 1.0 + 3.0 * x
        animation = FuncAnimation(
            self.fig,
            self.cobweb,
            frames=rs,
            interval=interval_ms,
            init_func=self.init_func,
            blit=True,
        )
        display(HTML(animation.to_jshtml()))
        plt.close("all")
