"""Tools for generating the Lambert fractals."""

import numpy as np
from matplotlib import pyplot as plt
import numba


@numba.vectorize(
    ["int32(complex128, complex128, float64, int_)"],
    cache=True,
    target="parallel",
)
def _escape_time(c, z0=0j, R=4.0, maxiter=100):
    """See escape_time.

    Numba does not seem to deal nicely with kwargs, defaults, etc. so we do that in a
    wrapper.  (This is strange though - should file an issue.)
    """
    z = z0
    for n in range(maxiter + 1):
        z = z * np.exp(z) + c
        if z.real > R:
            break
    return n


def escape_time(c, z0=0j, R=4.0, maxiter=100):
    """Return the "escape time" of initial point `z0` under `z->z*exp(z)+c`.

    Arguments
    ---------
    c : complex
        Parameter defining the Julia set or location in the Mandelbrot set.
    z0 : complex
        Location in the Julia set or parameter defining the Mandelbrot set.  (For values
        of `z0 != 0`, one gets a different Mandelbrot-like set.)
    R : float
        Bailout radius.
    maxiter : int
        Maximum number of iteration.  If the iteration does not terminate before this,
        then this value is returned.  It can be used as an approximation of the interior
        of the sets.
    """
    return _escape_time(c, z0, R, maxiter)
