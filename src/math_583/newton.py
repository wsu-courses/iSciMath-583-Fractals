"""Tools for generating the Newton fractals."""

import numpy as np
from matplotlib import pyplot as plt
import numba


# Since we want to pass in the roots as an array, we must generalize and use
# guvectorize, which means we must write our own loop.  Note also that even scalar
# arguments must be arrays
@numba.guvectorize(
    ["(complex128[:,:], complex128[:], float64[:], int_[:], int32[:,:])"],
    "(m,n),(i),(),()->(m,n)",
    cache=True,
    target="parallel",
)
def _convergence_time(z0, roots, eps, maxiter, n):
    for x in range(z0.shape[0]):
        for y in range(z0.shape[1]):
            z = z0[x, y]
            for n[x, y] in range(maxiter[0] + 1):
                g = 1 + 0j
                tmp = 0j
                for z_i in roots:
                    g *= z - z_i
                    tmp += 1 / (z - z_i)
                z -= 1 / tmp
                if g.real**2 + g.imag**2 <= eps[0]:
                    break


def convergence_time(z0, roots, eps=1e-8, maxiter=100):
    """Return the "convergence time" of initial point `z0` under Newton's method.

    Arguments
    ---------
    z0 : complex
        Location in the Julia set.
    roots : complex array
        Roots of the polynomial.
    eps : float
        Bailout radius.
    maxiter : int
        Maximum number of iteration.  If the iteration does not terminate before this,
        then this value is returned.  It can be used as an approximation of the interior
        of the sets.
    """
    return _convergence_time(z0, roots, eps, maxiter)


class NewtonWeb:
    """Class to draw and animate cobweb diagrams for Newton's method.

    Parameters
    ----------
    g : function
        Function to find roots of `g(x) == 0`. Must also return the derivative
        `dg_dx = g(x, d=1).
    x0 : float
        Initial guess.
    maxiter : int
        Maximum number of iterations.
    eps : float
        Bailout when abs(g(x)) < eps
    """

    def __init__(self, g, x0=1.0, maxiter=100, eps=1e-4):
        self.g = g
        self.x0 = x0
        self.maxiter = maxiter
        self.eps = eps
        self.ax = self.artists = None

    def f(self, x):
        g = self.g(x)
        dg = self.g(x, d=1)
        return x - g / dg

    def draw(self, ax=None):
        """Draw the diagram.

        Arguments
        ---------
        ax : Axis, None
            Axis to use for plotting.
        """
        # Generate population
        x0 = self.x0
        xs = [x0]
        for n in range(self.maxiter):
            x0 = self.f(x0)
            xs.append(x0)
            if abs(self.g(x0)) <= self.eps:
                break

        # Manipulate data for plotting
        Xs = np.empty((len(xs), 2))
        Ys = np.zeros((len(xs), 2))
        Xs[:, 0] = xs
        Xs[:, 1] = xs
        Ys[:, 0] = 0
        Ys[:, 1] = self.g(np.asarray(xs))
        Xs = Xs.ravel()  # [:-2]
        Ys = Ys.ravel()  # [:-2]

        xl, xh = np.min(xs), np.max(xs)
        dx = xh - xl
        x = np.linspace(xl - 0.1 * dx, xh + 0.1 * dx, 200)
        y = self.g(x)
        title = ""
        if ax is None:
            if self.ax is None:
                self.fig, self.ax = plt.subplots()
            ax = self.ax

        if self.artists is None:
            artists = self.artists = []
            artists.extend(ax.plot(Xs, Ys, "r", lw=0.5))
            artists.extend(ax.plot(x, y, "C0"))
            artists.extend(ax.plot(x, 0 * x, "k"))
            ax.set(title=title)
            artists.append(ax.title)
        else:
            artists = self.artists[:2] + self.artists[-1:]
            artists[0].set_data(Xs, Ys)
            artists[1].set_data(x, y)
            artists[2].set_text(title)
        self.Xs, self.Ys = Xs, Ys
        return artists
