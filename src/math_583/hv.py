"""Tools for plotting with HoloViews.

Note: to run these on CoCalc, one typically needs to run the independent Jupyter
Notebook server or Jupyter Lab - the CoCalc notebook interface does not support these
interactive plots.
"""
import numpy as np
import holoviews as hv
from holoviews import opts

hv.extension("bokeh")

# Customize the default tool bar a bit.   We want to make sure that the wheel_zoom is
# not active at the start (it prevents scrolling up and down in the page), and, on
# CoCalc we disable it for performance.
opts.defaults(
    opts.Image(
        tools=["reset", "pan", "box_zoom", "save"],  #'wheel_zoom'
        active_tools=["box_zoom"],
        aspect="equal",
    )
)


def plot(f, Nx=256 * 2, z0=-0.75, R=2, cmap="fire"):
    """Return an interactive HoloViews plot for f(z)."""

    def get_image(x_range, y_range, Nx=Nx):
        (x0, x1), (y0, y1) = x_range, y_range
        Ny = int(np.ceil(Nx * (y1 - y0) / (x1 - x0)))
        x, y = np.meshgrid(
            np.linspace(x0, x1, Nx), np.linspace(y0, y1, Ny), indexing="ij", sparse=True
        )
        z = x + 1j * y
        return hv.Image(f(z)[:, ::-1].T, bounds=(x0, y0, x1, y1))

    range_stream = hv.streams.RangeXY(
        x_range=(z0.real - R, z0.real + R), y_range=(z0.imag - R, z0.imag + R)
    )

    dmap = hv.DynamicMap(get_image, label="Manderbrot Explorer", streams=[range_stream])
    dmap.opts(opts.Image(height=Nx, width=Nx, xaxis=None, yaxis=None, cmap=cmap))
    return dmap
