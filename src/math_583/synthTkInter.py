"""TkInter version of the synthesizer.
"""
import time
import numpy as np
from tkinter import *
from tkinter import ttk

from synth import Synthesizer

'''
with Synthesizer(waveform='sin') as s:
    for n in np.arange(0, 25):
        s.pitch = 2 ** (n / 12)
        time.sleep(0.1)
exit()
'''

with Synthesizer(waveform='custom') as s:
    def update_freq(freq):
        s.pitch = float(freq) / s.freq_440_Hz

    root = Tk()
    root.title("Synthesizer")

    mainframe = ttk.Frame(root, padding="3 3 12 12")
    mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
    root.columnconfigure(0, weight=1)
    root.rowconfigure(0, weight=1)

    freq = StringVar()
    freq_entry = ttk.Scale(
        mainframe, length=200, from_=110.0, to=880.0, variable=freq, command=update_freq
    )
    freq_entry.grid(column=2, row=1)

    ttk.Label(mainframe, text="Freq. (Hz)").grid(column=3, row=1, sticky=W)
    ttk.Label(mainframe, textvariable=freq, width=10).grid(column=1, row=1, sticky=E)

    for child in mainframe.winfo_children():
        child.grid_configure(padx=5, pady=5)

    freq_entry.focus()

    root.mainloop()
