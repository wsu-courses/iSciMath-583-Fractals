"""Tools for generating the Mandelbrot and Julia sets."""

import numpy as np
from matplotlib import pyplot as plt
import numba


@numba.vectorize(
    ["int32(complex128, complex128, float64, int_)"],
    cache=True,
    target="parallel",
)
def _escape_time(c, z0=0j, R=4.0, maxiter=100):
    """See escape_time.

    Numba does not seem to deal nicely with kwargs, defaults, etc. so we do that in a
    wrapper.  (This is strange though - should file an issue.)
    """
    z = z0
    R2 = R**2
    for n in range(maxiter + 1):
        z = z**2 + c
        if z.real**2 + z.imag**2 > R2:  # Faster than abs(z) > R
            break
    return n


def escape_time(c, z0=0j, R=4.0, maxiter=100):
    """Return the "escape time" of initial point `z0` under `z->z**2+c`.

    Arguments
    ---------
    c : complex
        Parameter defining the Julia set or location in the Mandelbrot set.
    z0 : complex
        Location in the Julia set or parameter defining the Mandelbrot set.  (For values
        of `z0 != 0`, one gets a different Mandelbrot-like set.)
    R : float
        Bailout radius.
    maxiter : int
        Maximum number of iteration.  If the iteration does not terminate before this,
        then this value is returned.  It can be used as an approximation of the interior
        of the sets.
    """
    return _escape_time(c, z0, R, maxiter)


def draw_mandelbrot(
    c=-0.5 + 0j,
    Rx=2,
    Nx=256,
    Ny=None,
    ax=None,
    color_prep=lambda n: np.log10(1 + n),
    color_label=r"Escape time $\log_{10} n$",
    fancy=True,
    **kw,
):
    """Draw the Mandelbrot set using the escape-time algorithm.

    Arguments
    ---------
    c : complex
        Center point
    Rx : float
        "Radius" in the x direction.
    Nx, Ny : int
        Number of pixels in x and y (if `Ny == None`, then `Ny = Nx`).
    ax : Axis, None
        Axis to use.
    color_prep : function
        Applied to the escape time before coloring.
    color_label : str
        Label for colorbar.  Should reflect what `color_prep` does.
    fancy : bool
        If True, then include labels, a colorbar, and title.
    kw : dict
        Additional arguments are passed to escape_time.
    """
    if Ny is None:
        Ny = Nx
    Ry = Rx * Ny / Nx

    x = np.linspace(c.real - Rx, c.real + Rx, Nx)
    y = np.linspace(c.imag - Ry, c.imag + Ry, Ny)
    z = x[:, None] + 1j * y[None, :]
    n = escape_time(c=z, **kw)
    if ax is None:
        fig, ax = plt.subplots()
    else:
        fig = ax.figure

    mesh = ax.pcolormesh(x, y, np.log10(1 + n).T)
    ax.plot([c.real], [c.imag], "rx")
    ax.set(aspect=1)
    if fancy:
        fig.colorbar(mesh, label=color_label)
        ax.set(
            xlabel=r"$x = \Re z$",
            ylabel=r"$y = \Im z$",
            title=f"Mandelbrot set $M$ centered at $c={c}$",
        )
    return fig
