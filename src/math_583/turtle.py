"""Code related to Turtle geometry and L-systems."""
import numpy as np

__all__ = ["Lindenmayer", "walk", "draw"]


def walk(prog, z0=0, dz0=1.0, delta=90):
    """Return `(z, dz, paths)` implementing the specified program (L-system).

    Arguments
    ---------
    prog : str
        String of letters, '+', '-', and '[]' groupings.  Letters mean move forward by
        distance `dz` (the current direction), drawing if the letters are capitals.
        '+'/'-' means turn left/right by `delta` degrees.  Groupings mean save the
        state, then return after the grouping is complete.
    z0, dz0 : complex
        Initial position and direction.  The length of `dz0` determines the step size.
    delta : float
        Unit of rotation.

    Returns
    -------
    z, dz : complex
        Final position and direction.
    paths : list of lists
        List of connected paths to draw.
    """
    z = z0 + 0j  # Just in case z0 is mutable.
    dz = dz0 + 0j
    turn = np.exp(delta * 1j / 180 * np.pi)
    paths = []
    path = [z]
    stack = []
    for char in prog:
        if char == "[":
            stack.append((z, dz))
        elif char == "]":
            if not stack:
                raise ValueError("Unbalanced ']'")
            z, dz = stack.pop()
            if len(path) > 1:
                paths.append(path)
            path = [z]

        elif char == "+":
            dz *= turn
        elif char == "-":
            dz /= turn

        else:
            z += dz
            if char == char.upper():
                # Upper characters draw
                path.append(z)
            else:
                # Lower case move
                if len(path) > 1:
                    paths.append(path)
                path = [z]
    if len(path) > 1:
        paths.append(path)
    return z, dz, paths


def draw(paths, ax=None, ls="-", c="k", **kw):
    """Draw the path to the specified axes."""
    if ax is None:
        import matplotlib.pyplot as plt

        ax = plt.gca()
    ax.set(aspect=1)
    for path in paths:
        zs = np.asarray(path)
        ax.plot(zs.real, zs.imag, ls=ls, c=c, **kw)
    return ax


class Lindenmayer:
    """Lindenmayer systems (L-systems).

    L-systems consist of a tuple `(V, axiom, rules)` where `V` is the alphabet -- here
    consisting of:

    * Letters: Move forward.  Uppercase letters mean draw.
    * '+', '-': Turn left/right by `delta` degrees.
    * '[', ']': Groupings.  These start and end an excursion, returning to the position
                and heading at the start of the group.

    The system consists of `N` parallel replacement using the `rules` dictionary
    starting from the `axiom`.

    Parameters
    ----------
    delta : float
        Angle of rotation in degrees.

    axiom : str
        Initial string.  Use to place copies of the recursive system.
    rules : dict
        Dictionary of replacement rules.

    Attributes
    ----------
    s : str
        The final string.
    paths : list of lists
        List of connected paths.
    """

    delta = 90  # Turn angle (degrees)
    axiom = "F"  # Initial string
    rules = dict(f="f", F="F")

    def __init__(self, delta=90, axiom="F", **rules):
        self.delta = delta
        self.axiom = axiom
        self.rules = rules

    def __repr__(self):
        delta, axiom = self.delta, self.axiom
        rules = ", ".join(f"{key}='{self.rules[key]}'" for key in self.rules)
        return f"{self.__class__.__name__}({delta=}, {axiom=}, {rules})"

    def compute(self, N):
        """Return the string corresponding to `N` replacements."""
        s = self.axiom
        for n in range(N):
            s = "".join([self.rules.get(c, c) for c in s])
        return s

    def walk(self, s, z0=None, dz0=None):
        """Return `paths` after walking string `s`.

        Arguments
        ---------
        z0, dz0 : complex
            Initial position and direction.  The length of `dz0` determines the step size.
        """
        if z0 is None:
            z0 = 0.0
        if dz0 is None:
            dz0 = 1.0
        z, dz, paths = walk(s, z0=z0, dz0=dz0, delta=self.delta)
        return paths

    def draw(self, N, z0=0, dz0=1.0, **kw):
        """Compute and draw the Nth path."""
        s = self.compute(N=N)
        paths = self.walk(s, z0=z0, dz0=dz0)
        return draw(paths=paths, **kw)
