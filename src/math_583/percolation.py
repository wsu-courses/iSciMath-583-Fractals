import numpy as np, matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from numba import njit, prange

# 0: No tree
# 1: Living tree
# 2: Burning tree
# 3: Burnt tree

NO_TREE = 0
LIVING = 1
BURNING = 2
BURNT = 3

# Some plotting routines
cm = ListedColormap(["black", "green", "red", "grey"])


def draw(X, ax=None):
    if ax is None:
        ax = plt.gca()
    ax.imshow(X, origin="lower", interpolation="none", cmap=cm, vmin=0, vmax=3)
    ax.set(aspect=1, ylabel="$n$")


@njit(parallel=True)
def update(X_):
    """Nearest-neighbours (4)"""
    Nx, Ny = X_[1:-1, 1:-1].shape
    X1_ = X_.copy()
    for nx in prange(1, Nx + 1):
        for ny in range(1, Ny + 1):
            if X_[nx, ny] == BURNING:  # If tree is on fire..
                X1_[nx, ny] = BURNT  # ...tree is burnt next step.
            elif X_[
                nx, ny
            ] == LIVING and (  # Otherwise, living trees next to burning trees...
                X_[nx - 1, ny] == BURNING
                or X_[nx + 1, ny] == BURNING
                or X_[nx, ny - 1] == BURNING
                or X_[nx, ny + 1] == BURNING
            ):
                X1_[nx, ny] = BURNING  # start burning.
    # Periodic boundaries
    for nx in prange(1, Nx + 1):
        X1_[nx, 0] = X1_[nx, -2]
        X1_[nx, -1] = X1_[nx, 1]
    return X1_


@njit(parallel=True)
def update2(X_):
    """Extended nearest-neighbours (8)"""
    Nx, Ny = X_[1:-1, 1:-1].shape
    X1_ = X_.copy()
    for nx in prange(1, Nx + 1):
        for ny in range(1, Ny + 1):
            if X_[nx, ny] == BURNING:  # If tree is on fire..
                X1_[nx, ny] = BURNT  # ...tree is burnt next step.
            elif X_[
                nx, ny
            ] == LIVING and (  # Otherwise, living trees next to burning trees...
                X_[nx - 1, ny] == BURNING
                or X_[nx + 1, ny] == BURNING
                or X_[nx, ny - 1] == BURNING
                or X_[nx, ny + 1] == BURNING
                # Include these if you want diagonal neighbours for a new problem...
                or X_[nx - 1, ny + 1] == BURNING
                or X_[nx + 1, ny + 1] == BURNING
                or X_[nx - 1, ny - 1] == BURNING
                or X_[nx - 1, ny + 1] == BURNING
            ):
                X1_[nx, ny] = BURNING  # start burning.
    # Periodic boundaries
    for nx in prange(1, Nx + 1):
        X1_[nx, 0] = X1_[nx, -2]
        X1_[nx, -1] = X1_[nx, 1]
    return X1_


def evolve(Nxy=(100, 100), p=0.5927460507921, seed=3, update=update):
    """Return the stages `X_` until the fire goes out."""
    Nx, Ny = Nxy
    rng = np.random.default_rng(seed=seed)
    X = rng.choice([NO_TREE, LIVING], size=Nxy, p=[1 - p, p])

    # Make slightly large array to add ghost-points to simplify testing
    X_ = np.zeros((Nx + 2, Ny + 2))
    X_[1:-1, 1:-1] = X

    # Periodic boundary conditions
    X_[:, 0] = X_[:, -2]
    X_[:, -1] = X_[:, 1]

    # Burning top row
    X_[1, :] *= BURNING

    Xs = [X_[1:-1, 1:-1].copy()]
    while np.any(X_[1:-1, 1:-1] == BURNING):
        # Iterate until there are no more burning trees.
        X_ = update(X_)
        Xs.append(X_[1:-1, 1:-1].copy())

    return np.asarray(Xs)


def get_Z(Xs, p):
    """Return the order parameter."""
    Nt, Nx, L = Xs.shape
    Z = (Xs == BURNT).sum(axis=-1) / (p * L)
    return Z
