"""Tools for generating fractal sets."""
from itertools import product

import numpy as np

from .turtle import Lindenmayer

__all__ = [
    "get_cantor",
    "get_twindragon",
    "get_point_attractor",
    "get_sierpinski",
    "Lindenmayer",
    "z2xy",
]


def get_cantor(N, terminal_1=True, base=3, allowed_digits=(0, 2)):
    """Return elements of the Cantor set using base-3 fractions.

    Arguments
    ---------
    N : int
       Return all ternary fractions with N digits in the Cantor set.  These are the
       fractions in base 3 that contain no digit 1.
    terminal_1: bool
       If `True`, then include fractions with a terminal 1.  I.e. `0.1 = 0.022222...`.
    """
    q = 1 / (base ** np.arange(1, N + 1))
    digits = [
        allowed_digits,
    ] * (N - 1)
    if terminal_1 and base - 1 in allowed_digits and 1 not in allowed_digits:
        digits.append(sorted(list(allowed_digits) + [1]))
    else:
        digits.append(allowed_digits)
    cs = [sum(d * q) for d in product(*digits)]
    if terminal_1:
        cs += [1]
    return np.asarray(cs)


def get_twindragon(N, base=1 - 1j):
    """Return elements of the twwindragon using proper fractions in base `(1-i)`.

    Arguments
    ---------
    N : int
        Number of digits in the fractions.
    base : complex
        Base to use.  The figure in the book and on the Wikipedia page use `i-1` instead
        which fills a slightly different region of the plane.
    """
    q = 1 / base
    points = np.array([0])
    N = 13

    for n in range(1, N):
        new_points = points + q
        points = np.concatenate([points, new_points])
        q /= base

    return points


def z2xy(z):
    """Return `(x, y)` array for complex points `z`."""
    return np.einsum(
        "...j->j...",
        np.asarray(z, dtype=complex).view(dtype=float).reshape(z.shape + (2,)),
    )


def get_point_attractor(
    N,
    attractors,
    weights=1 / 2,
    rotations=1,
    probabilities=None,
    seed=2,
    z0=None,
    Nskip=0,
):
    """Return `N` points of the IFS using the inverse iteration method.

    Arguments
    ---------
    N : int
        Number of points to return.
    attractors : complex array
        List of attracting points.
    weights : complex, complex array
        Weights for each attractor.  The rule is to move "distance" `w` toward the
        chosen attractor.  Precisely `z -> r*z + w*(a-r*z)` which allows for some rotation
        if the weights are chosen to be complex.
    rotations : complex, complex array
        Rotation matrices and pre-scaling.
    probabilities : None, float array
        Probabilities for each move.  If `None`, then they will be chosen so each weight
        gets an equal density `probabilities = 1/abs(weights)`.
    seed : int
        Seed for random number generator.
    z0 : complex, None
        Initial point.  (Uses a vertex if None).
    Nskip : int
        Number of points to skip before collecting.
    """
    # Use a properly seeded random number generator so we can reproduce our results.
    rng = np.random.default_rng(seed=seed)

    a = np.asarray(attractors)
    w = np.ones(len(a)) * weights
    r = np.ones(len(a)) * rotations
    if probabilities is None:
        p = 1 / abs(w)
    else:
        p = np.asarray(probabilities)
    p = p / np.sum(p)

    if z0 is None:
        z0 = a[0]

    z = z0
    zs = []
    inds = rng.choice(len(p), p=p, size=Nskip + N)
    for n, ind in enumerate(inds):
        z = (r * z + w * (a - r * z))[ind]
        if n > Nskip:
            zs.append(z)
    return np.asarray(zs)


def get_sierpinski(N, **kw):
    ths = np.pi / 2 + 2 * np.pi * np.arange(3) / 3
    attractors = np.exp(1j * ths)
    weights = 1 / 2
    return get_point_attractor(N=N, attractors=attractors, weights=weights, **kw)


'''
def sirpinski(
    N,
    z0=ps[0],
    Nskip=0,
    rng=rng,
):
    """Generate N points randomly moving towards the Sirpinski attractor.

    Arguments
    ---------
    N : int
        Points to generate
    z0 : complex
        Initial point
    Nskip : int
        Number of points to skip.
    """
    if fast:
        n = j = np.arange(N + Nskip)
        pj = ps[rng.integers(3, size=N + Nskip)]
        return (z0 + np.cumsum(pj * 2**j)) / 2**n
    else:
        z = z0
        zs = []
        for n in range(Nskip + N):
            z = (z + ps[rng.integers(3)]) / 2
            if n > Nskip:
                zs.append(z)
        return np.asarray(zs)


ax.plot(*z2xy(ps), "ok")
ax.plot(*z2xy(sirpinski(10000)), ".", ms=1)
ax.set(xlabel="$x$", ylabel="$y$", aspect=1)
'''
