"""Iterated Function Systems.
"""

import inspect

import numpy as np


class IFS:
    """Subclass this defining a set of functions `f<n>`.  See `f0()`"""

    seed = 2  # Seed for default_rng
    N0 = 100  # Number of iterations of f0(0) to get z0

    def __init__(self, **kw):
        for key in kw:
            if not hasattr(self, key):
                raise ValueError(f"Unknown {key=}")
            setattr(self, key, kw[key])
        self.init()

    def f0(self, z):
        """Return the mapping as a function of `z=x+1j*y`.

        Set `f0.weight` to the relative weight for random generation.
        """
        return z

    f0.weight = 1

    def init(self):
        """Compute any initializations here."""
        members = inspect.getmembers(self)
        self._fs = [f for (name, f) in members if name.startswith("f")]
        weights = [getattr(f, "weight", 1) for f in self._fs]
        weights = np.maximum(0.01 * np.max(weights), weights)
        self._ps = np.divide(weights, np.sum(weights))
        self._rng = np.random.default_rng(self.seed)
        z0 = 0.0
        for n in range(self.N0):
            z0 = self.f0(z0)
        self.z0 = z0

    def get_ifs(self, N=1000):
        """Return N points in the random IFS."""

        fs = self._rng.choice(self._fs, size=N, replace=True, p=self._ps)
        z = self.z0
        return np.array([z := f(z) for f in fs])

    def __call__(self, zs):
        """Apply IFS to points `zs` and return new points."""
        zs = np.asarray(zs)
        return np.concatenate([f(zs) for f in self._fs])

    def plot(self, zs, ax=None, aspect=1, ms=1, **kw):
        """Plot the points `zs` in axis `ax` and return `ax`."""
        zs = np.asarray(zs)
        if ax is None:
            import matplotlib.pyplot as plt

            ax = plt.gca()
        ax.plot(zs.real, zs.imag, ".", ms=ms, **kw)
        ax.set(aspect=aspect)
        return ax

    def draw(self, N=1000, images=True, ax=None, **kw):
        """Compute and draw N points of the IFS.

        Arguments
        ---------
        N : int
            Number of points.
        images : bool
            If True, then draw the images of the final set (in different colors).
        """
        if ax is None:
            import matplotlib.pyplot as plt

            ax = plt.gca()

        zs = self.get_ifs(N=N)
        if not images:
            self.plot(zs, ax=ax, **kw)
        else:
            for f in self._fs:
                self.plot(f(zs), ax=ax, **kw)
        return ax


class IFS1(IFS):
    """Version of IFS based on The Science of Fractal Images table 5.1.

    f(z) = s*z + (1-s)*a

    """

    s = np.array([0.6, 0.6, 0.4 - 0.3j, 0.4 + 0.3j])
    a = np.array([0.45 + 0.9j, 0.45 + 0.3j, 0.6 + 0.3j, 0.3 + 0.3j])

    def init(self):
        for n, (s, a) in enumerate(zip(self.s, self.a)):
            name = f"f{n}"

            def f(z, _s=s, _a=a):
                return _s * z + (1 - _s) * _a

            f.weight = abs(s)
            setattr(self, name, f)
        super().init()


class IFS2(IFS):
    """General linear IFS.  Each transform is a 2x3 matrix acting on [x, y, 1]."""

    Ms = np.array(
        [
            [
                [0.5, 0, 0],
                [0, 0.5, 0],
            ],
            [
                [0.5, 0, 0.5],
                [0, 0.5, 0],
            ],
            [
                [0.5, 0, 0],
                [0, 0.5, 0.5],
            ],
        ]
    )

    def init(self):
        for n, M in enumerate(self.Ms):
            name = f"f{n}"

            def f(z, _M=M):
                z = np.ravel(z)
                xy1 = np.array([z.real, z.imag, np.ones(z.shape)])
                x, y = _M @ xy1
                return x + 1j * y

            f.weight = abs(np.linalg.det(M[:2, :2]))
            setattr(self, name, f)
        super().init()


class IFSBarnsley(IFS):
    """Version of IFS based on The Science of Fractal Images table 5.2."""

    # Translation, Rotation (deg), Scale
    trans_rots_scales = np.array(
        [
            # [(h, k), (θ, ψ), (r, s)]
            [[0, 0], [0, 0], [0, 0.16]],
            [[0, 1.6], [-2.5, -2.5], [0.85, 0.85]],
            [[0, 1.6], [49, 49], [0.3, 0.3]],
            [[0, 0.44], [120, -50], [0.3, 0.37]],
        ]
    )

    def init(self):
        for n, (trans, rots, scales) in enumerate(self.trans_rots_scales):
            name = f"f{n}"
            r, s = scales
            theta, psi = np.pi * rots / 180
            M = np.array(
                [
                    [r * np.cos(theta), -s * np.sin(psi)],
                    [r * np.sin(theta), s * np.cos(psi)],
                ]
            )

            def f(z, _M=M, _hk=trans):
                z = np.ravel(z)
                xy = np.array([z.real, z.imag])
                x, y = _M @ xy + _hk[:, None]
                return x + 1j * y

            f.weight = abs(np.linalg.det(M))
            setattr(self, name, f)
        super().init()


class IFSPeitgen53(IFS2):
    """Version of IFS in section 5.3 of Peitgen:2004."""

    # Choose from one of the 8 operation sin fig 5.18
    # number 0 through 7
    ds = [0, 0, 0]

    trans = [(0, 0.5), (0, 0), (0.5, 0)]
    angle_sy = {
        0: (0, 1),
        1: (90, 1),
        2: (180, 1),
        3: (-90, 1),
        4: (0, -1),
        5: (180, -1),
        6: (90, -1),
        7: (-90, -1),
    }

    def init(self):
        Ms = []
        for d, t in zip(self.ds, self.trans):
            angle, sy = self.angle_sy[d]
            th = angle * np.pi / 180
            R = np.array(
                [
                    [np.cos(th), -sy * np.sin(th)],
                    [np.sin(th), sy * np.cos(th)],
                ]
            )
            trans = np.transpose([-R @ [0.25, 0.25] + [0.25, 0.25] + t])
            Ms.append(np.hstack([0.5 * R, trans]))
        self.Ms = np.array(Ms)
        super().init()


class IFSSuperFractal(IFS):
    """IFS from Barnsley's SuperFractals book (see Table 0.1).

    $$
      f(x, y) = \begin{pmatrix}
        \frac{ax + by + c}{gx + hy + j},
        \frac{dx + ey + k}{gx + hy + j}
      \end{pmatrix}
    $$
    """

    abcdekghj = [
        [19.05, 0.72, 1.86, -0.15, 16.9, -0.28, 5.63, 2.01, 20.0],
        [0.2, 4.4, 7.5, -0.3, -4.4, -10.4, 0.2, 8.8, 15.4],
        [96.5, 35.2, 5.8, -131.4, -6.5, 19.1, 134.8, 30.7, 7.5],
        [-32.5, 5.81, -2.9, 122.9, -0.1, -19.9, -128.1, -24.3, -5.8],
    ]
    weights = [60, 0.1, 20, 19]

    def init(self):
        for n, (abcdekghj, weight) in enumerate(zip(self.abcdekghj, self.weights)):
            name = f"f{n}"

            def f(z, _abcdekghj=abcdekghj):
                (a, b, c, d, e, k, g, h, j) = _abcdekghj
                z = np.ravel(z)
                x, y = np.array([z.real, z.imag])
                denom = g * x + h * y + j
                x, y = (
                    (a * x + b * y + c) / denom,
                    (d * x + e * y + k) / denom,
                )
                return x + 1j * y

            f.weight = weight
            setattr(self, name, f)
        super().init()
