---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.6
kernelspec:
  display_name: Python 3 (math-583)
  language: python
  name: math-583
---

```{code-cell}
:tags: [hide-cell]

import mmf_setup; mmf_setup.nbinit()
import os
from pathlib import Path
import logging
logging.getLogger("matplotlib").setLevel(logging.CRITICAL)
logging.getLogger('numba').setLevel(logging.CRITICAL)
logging.getLogger('OMP').setLevel(logging.CRITICAL)
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
```

(sec:Assignment6)=
# Assignment 6: Feigenbaum Constant and the Logistic Map

The goal here is to compute the [Feigenbaum
constant](https://en.wikipedia.org/wiki/Feigenbaum_constants) (sometimes called the
Feigenvalue). 

\begin{gather*}
  \delta \approx 4.669.
\end{gather*}

## Mandelbrot Set

As discussed in the following Veritasium video (part of the readings on
[Discourse](https://discourse.iscimath.org/t/mathematics-583-analysis-of-and-on-rough-sets/967/8]),
the Feigenbaum constant can be defined as the limit of the ratio of distances between
the various bulbs on the left of the Mandelbrot set.

<iframe width="560" height="315"
src="https://www.youtube.com/embed/ovJcsL7vyrk?si=I9dUo6wWmLJThBtP" title="YouTube video
player" frameborder="0" allow="accelerometer; autoplay; clipboard-write;
encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen>
</iframe> 

```{code-cell}
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt

from math_583.mandelbrot import draw_mandelbrot

# Here you can play with trying to locate the bifurcation points.
cs = [-0.75, -1.25, -1.37, -1.394]
Rxs = [1.0, 0.5, 0.1, 0.01]
N = len(cs)
fig, axs = plt.subplots(1, N, figsize=(4*N, 4))
for ax, c, Rx in zip(axs, cs, Rxs):
    draw_mandelbrot(c=c, Rx=Rx, ax=ax, fancy=False, maxiter=1000)
    ax.axis("off")

# Here are the ratios of the successive differences. 
# How does this relate to the Feigenbaum constant? 
dc = np.diff(cs)
print(f"Differences = {dc=}")
dc[1:] / dc[:-1]
```

Can you semi-automate this process?  I.e. can you use your results to try to guess the
next value of $c$ and $R_x$?  You should be able to narrow in on the value quite quickly
with some thought.

## Logistic Equation

A simpler (equivalent) way to obtain the Feigenbaum constant is to consider the map (see
the {ref}`sec:LogisticMap` for details).

\begin{gather*}
  x \mapsto f_r(x) = r x(1-x),
\end{gather*}

Depending on the value of $r$, this may have stable fixed points of various periods $p$:
$f_r^{(p)}(x) = x$.  Starting from small $r$, one finds a single fixed point of
period 1.  At a critical value $r_2$, this bifurcates to give a stable orbit of period
$2$.  Increasing $r$ further gives another bifurcation at $r_4$ to a period 4 orbit.
This "period doubling" is very common, and one can define the Feigenbaum constant as the
ratio of the distances

\begin{gather*}
  \delta = \lim_{n\rightarrow\infty} \frac{r_{2^{n}} - r_{2^{n-1}}}{r_{2^{n+1}} - r_{2^{n}}}
\end{gather*}


```{code-cell}
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt

def f(x, r, d=0):
    """Logistic growth function (or derivative)"""
    if d == 0:
        return r*x*(1-x)
    elif d == 1:
        return r*(1-2*x)
    elif d == 2:
        return -2*r
    else:
        return 0


x = np.linspace(0, 1)

fig, ax = plt.subplots(figsize=(4, 2))
ax.plot(x, x, '-k')
for r in [1.0, 2.0, 3.0, 4.0]:
    ax.plot(x, f(x, r=r), label=f"$r={r}$")
ax.legend()
ax.set(xlabel="$x$", ylabel="$f_r(x)$");
```

### Questions

Play with this and determine values for the bifurcation points $r_{2}$, $r_{4}$, $r_{8}$
etc.  Use these to approximate the Feigenbaum constant.

:::{margin}
The following uses `interact`, and so will not be run in the documents.
:::
```python
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt

from ipywidgets import interact

from math_583.plotting import CobWeb

def f(x, r):
    return r*x*(1-x)

fig, ax = plt.subplots(figsize=(8, 3))
c = CobWeb(f)
plt.close('all')

@interact(r=(1, 4, 0.001))
def draw(r=1):
    c.cobweb(r, ax=ax)
    return fig
```

## Bifurcation Plot

```{code-cell}
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt

def f(x, r):
    return r*x*(1-x)

r0, r1 = 1.0, 4.0  # Range of rs
x0 = 0.2   # Initial point
Nr = 1000  # Number of rs to use
N = 1000   # Number of points to plot.
N0 = 100   # Number of initial points to skip

fig, ax = plt.subplots()

rs = np.linspace(r0, r1, Nr)
for r in rs:
    x = x0
    for n in range(N0):
        x = f(x, r=r)
    xs = []
    for n in range(N):
        x = f(x, r=r)
        xs.append(x)
    ax.plot([r]*len(xs), xs, '.', ms=0.1, c='k')
ax.set(xlabel='$r$', ylabel='$x$');
```
