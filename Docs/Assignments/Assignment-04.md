---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.6
kernelspec:
  display_name: Python 3 (math-583)
  language: python
  name: math-583
---

```{code-cell}
:tags: [hide-cell]

import mmf_setup; mmf_setup.nbinit()
import os
from pathlib import Path
import logging
logging.getLogger("matplotlib").setLevel(logging.CRITICAL)
logging.getLogger('numba').setLevel(logging.CRITICAL)
logging.getLogger('OMP').setLevel(logging.CRITICAL)
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
```

(sec:Assignment4)=
# Assignment 4: Mandelbrot Set

The goal of this assignment is to explore the {ref}`sec:Mandelbrot` $M$ for $z^2+c$.  Note,
both $M$ and its boundary $\partial M$ have Hausdorff dimension 2.  Can you verify this
numerically?  *(This might be quite challenging to do, but might be possible with the
BSM algorithm.)*


## Exploration

To get a feel for the structure of Mandelbrot set, you can play with the following code.
This uses a compiled version of the escape-time algorithm which you should implement
below.  For now, you can play a bit:

Note: this is almost identical to the code used for generating Julia sets.  The only
difference is that we now set `c=z` which varies over the image and we set `z0=0` which
is now held fixed.

In this way, the Mandelbrot set explores what happens to the origin `z0=0+0j` in the
Julia set $J_c$.

```{code-cell}
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt

from math_583.mandelbrot import escape_time

# Try changing the scale and c here.  We center the image on c
c = 0.264999+0.00349999999j
R, maxiter = 2.0, 100
#R, maxiter = 0.0001, 2000

x = np.linspace(c.real - R, c.real + R, 1024)
y = np.linspace(c.imag - R, c.imag + R, 1024)
z = x[:, None] + 1j*y[None, :]

# Try changing maxiter - especially if you get deep.
%time n = escape_time(c=z, z0=0.0, maxiter=maxiter)

fig, ax = plt.subplots()
mesh = ax.pcolormesh(x, y, np.log10(1+n).T, shading='auto')
ax.plot([c.real], [c.imag], 'rx')
fig.colorbar(mesh, label=r"Escape time $\log_{10} n$")
ax.set(xlabel=r"$x = \Re z$", ylabel=r"$y = \Im z$", 
       title=f"Mandelbrot set $M$ centered at $c={c}$", 
       aspect=1);
```

## [XaoS][]

The code here works, but the UX is a bit cludgy.  You might want to instead download
[XaoS][] and play with it.  Note: the default there is to explore the [Mandelbrot set][]
$M$, which we will do next assignment.  The set $M$ provides a classification for the
different Julia sets.  To see the corresponding Julia set $J_c$, use the `m`
([Mandelbrot/Julia switching][]) or `j` ([Fast Julia preview mode][]) keyboard
shortcuts.

## Coding Suggestions

### Escape-Time Algorithm

Here is a simple python version of the previous algorithm.  It is very slow, but you can
have complete control over it.  Can you think how to make the algorithm faster (not
simply by compiling)?  If you have any substantial improvements, we can work with you to
implement those in a compiled version.

```{code-cell}
def mandelbrot_escape_time(z0=0+0j,
                           R=4.0,
                           maxiter=100,
                           x_range=(-2, 2),
                           y_range=(-2, 2),
                           Nx=200,
                           Ny=200):
    """Return `(x, y, res)` where `res` is the number iterations to "escape".

    Arguments
    ---------
    R : float
        Bailout radius.  We say that a point has "escaped" once `abs(z) > R`.
    maxiter : int
        Maximum number of iterations.  We say that a point has not escaped if `abs(z) < R`
        for `maxiter` iterations.  Such points provide an approximation of the filled-in
        Julia set `K_c` and will have `n == maxiter`.

    z0 : complex
        Starting point.
    x_range, y_range : (float, float)
        Range to plot: `x_range = (x[0], x[-1])` etc.
    Nx, Ny : int
        Number of pixels in each direction.  `res.shape == (Nx, Ny)`.

    Results
    -------
    x, y : float arrays
        1D arrays of the cell boundaries.  Note that these should have `Nx` and `Ny`
        elements because they are cell boundaries.  See the code below for how to do this.
    res : 2D int array
        This is an array of integers expressing how many iterations it takes the initial
        point `z=x+j*y` to escape `abs(z) > R`.  Note that `res.shape = (Nx, Ny)`.
    """

    # Some code to get you started... but feel free to explore other techniques.
    x = np.linspace(*x_range, Nx)  # These are the pixel centers, not the boundaries.
    y = np.linspace(*y_range, Ny)
    res = np.zeros((Nx, Ny), dtype=int)

    # Your code here.  Possibly something based on the following loop, but this will be
    # slow.  I recommend starting with a loop, but then trying to vectorize or optimize
    # your code later.
    for nx in range(Nx):
        for ny in range(Ny):
            #######################
            # Calculate res[nx, ny]
            #
            # Your code here, but this works.
            c = x[nx] + 1j * y[ny]
            z = z0
            for n in range(maxiter + 1):
                z = z**2 + c
                if abs(z) > R:
                    break
            res[nx, ny] = n
    return x, y, res
```

Here is how this function could be used to generate a plot similar to that above:

```{code-cell}
args = dict(maxiter=100, x_range=(-2, 0.5), y_range=(-1.2, 1.2), Nx=500, Ny=500)
%time x, y, res = mandelbrot_escape_time(**args)

fig, ax = plt.subplots()
# Shading needed when x, y give cell centers instead of boundaries
# The transpose is because we are not using meshgrid (image/MATLAB compatibility).
mesh = ax.pcolormesh(x, y, np.log10(res).T, shading='auto')
fig.colorbar(mesh, label=r"Escape time $\log_{10} n$")
ax.set(xlabel=r"$x = \Re z$",
       ylabel=r"$y = \Im z$",
       title=f"Mandelbrot Set",
       aspect=1);
```

### BSM - Boundary Scanning Method

The previous escape-time algorithm is useful for determining the Mandelbort set $M$.
This fills solid regions of space, such as the main cardioid, and thus has Hasudorff
dimension 2.  What about the boundary $\partial M$?

The goal here is to determine whether or not a cell $(x, y) \in [x_0, x_1] \times [y_0,
y_1]$ contains an element $z \in \partial M$ on the boundary of the Mandelbrot set.


```{code-cell}
def get_mandelbrot_boundary(x_range=(-2, 2), y_range=(-2, 2), Nx=200, Ny=200):
    """Return `(x, y, res)` where `res` is 1 if the corresponding cell contains points in $J_c$.

    Arguments
    ---------
    x_range, y_range : (float, float)
        Range to plot: `x_range = (x[0], x[-1])` etc.
    Nx, Ny : int
        Number of pixels in each direction.  `res.shape == (Nx, Ny)`.

    Results
    -------
    x, y : float arrays
        1D arrays of the cell boundaries.  Note that these should have `Nx+1` and `Ny+1`
        elements because they are cell boundaries.  See the code below for how to do this.
    res : 2D bool array
        This is 1 where the corresponding cell contains points of the Julia set and 0
        elsewhere.  Note that `res.shape = (Nx, Ny)`.
    """

    # Some code to get you started... but feel free to explore other techniques.
    x = np.linspace(*x_range, Nx + 1)  # Add one extra point for boundaries
    y = np.linspace(*y_range, Ny + 1)
    res = np.zeros((Nx, Ny), dtype=bool)  # Bool arrays save a little space.

    # Your code here.  Possibly something based on the following loop, but this will be
    # slow.  I recommend starting with a loop, but then trying to vectorize or optimize
    # your code later.
    for nx in range(Nx):
        for ny in range(Ny):
            x0, x1 = x[nx:nx + 2]  # Boundary of cell.
            y0, y1 = y[ny:ny + 2]

            #####################
            # Calculate f[nx, ny]
            #
            # Your code here.
            res[nx, ny] = True

    return x, y, res
```

Once you have this working, you should be able to apply the BSM box-counting technique
(see {ref}`sec:BSMBoxCounting`).

## Followup Questions

### Cardioids

As discussed in [one of 3 Blue 1 Brown's
videos](https://www.youtube.com/watch?v=LqbZpur38nw&t=1s&ab_channel=3Blue1Brown), the
main cardioid of the Mandelbrot set consists of all values of $c$ for which the
iteration converges to a single attractive fixed point.  Derive the equation for the
boundary of this cardioid and show that it is equivalent to the following: 

\begin{gather*}
  \def\I{\mathrm{i}}
  c(t) = \frac{e^{2\pi \I t}(2 - e^{2\pi \I t})}{4}, \qquad t \in [0, 1).
\end{gather*}

*(See the section on {ref}`sec:Julia` if you need a hint.)*

Apparently very little is known about the Julia sets for irrational values of $t$,
especially those that are not easily approximated by simple fractions.

Can you generalize this to bound the regions that have stable attractors with period
$2$, $3$, etc.? 

Plot your solutions on the Mandelbrot set to check.  Below is a function that draws the
Mandelbrot set you can use.  I have checked the above formula.

```{code-cell}
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
from math_583.mandelbrot import escape_time

def draw_mandelbrot(c=-0.75, R=1.5, Nx=256, maxiter=100, ax=None):
    """Draw the Mandelbrot set centered on c."""
    x = np.linspace(c.real - R, c.real + R, Nx)
    y = np.linspace(c.imag - R, c.imag + R, Nx)
    z = x[:, None] + 1j*y[None, :]
    n = escape_time(c=z, z0=0.0, maxiter=maxiter)
    if ax is None:
        fig, ax = plt.subplots()
    fig = ax.figure
    mesh = ax.pcolormesh(x, y, np.log10(1+n).T, shading='auto')
    ax.plot([c.real], [c.imag], 'rx')
    fig.colorbar(mesh, label=r"Escape time $\log_{10} n$")
    ax.set(xlabel=r"$x = \Re z$", ylabel=r"$y = \Im z$", 
           title=f"Mandelbrot set $M$ centered at $c={c}$", 
           aspect=1);
    return ax

ax = draw_mandelbrot()
t = np.linspace(0, 1)
q = np.exp(2j*np.pi*t)
c1 = q*(2-q)/4
ax.plot(c1.real, c1.imag, '-r');

# Plot some rational points.  Notice that this is where the bulbs start.
for denom in [1, 2, 3, 4, 5]:
    t = np.array([numer/denom for numer in range(denom)])
    q = np.exp(2j*np.pi*t)
    c1 = q*(2-q)/4
    ax.plot(c1.real, c1.imag, '+', label=f"$n/{denom}$")
ax.legend();
```


### A Strange Coincience?
​
How long does it take for $z_0=0$ to escape to $|z|>4$ given $c=-3/4 + \epsilon
\mathrm{i}$ for $\epsilon \in \{1, 0.1, 0.01, 0.001, 0.0001, \dots\}$?  Do you notice
anything? Note that the successive digits form the series
[A000796](https://oeis.org/A000796) in the Online Encyclopedia of Integer Sequences.
Why?  Is this a coincidence?

```{code-cell}
for eps in 1/10**np.arange(8):
    print(escape_time(c=-0.75 + eps*1j, z0=0, maxiter=100000000, R=4))
```

[Xaos]: <https://xaos-project.github.io/index.html>
[Mandelbrot/Julia switching]: <https://github.com/xaos-project/XaoS/wiki#mandelbrotjulia-switching>
[Fast Julia preview mode]: <https://github.com/xaos-project/XaoS/wiki#fast-julia-preview-mode>
