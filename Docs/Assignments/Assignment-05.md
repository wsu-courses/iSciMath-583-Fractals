---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.6
kernelspec:
  display_name: Python 3 (math-583)
  language: python
  name: math-583
---

```{code-cell}
:tags: [hide-cell]

import mmf_setup; mmf_setup.nbinit()
import os
from pathlib import Path
import logging
logging.getLogger("matplotlib").setLevel(logging.CRITICAL)
logging.getLogger('numba').setLevel(logging.CRITICAL)
logging.getLogger('OMP').setLevel(logging.CRITICAL)
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
```

(sec:Assignment5)=
# Assignment 5: Box Counting Dimension

Start writing your own code in `~/MyCode` to compute the dimension, measure, and set
reconstruction from samples. 

Here is the basic idea: we have included symbolic links to both your code (in the
[`~/MyCode`](MyCode) folder) and the course `math_583` package.

:::{warning}
The following requires code to be deposited into `~/MyCode` which we do on
CoCalc, but not here, so the following will not execute in the documents.)*
:::

```python
# Simple example: import get_cantor from the course repository
from math_583.fractals import get_cantor

# Now import your code
import MyCode.box_counting

# Get samples from the cantor set
c = get_cantor(4)

# Use your code to count the number of points in 10 bins
MyCode.box_counting.count_box1(c, bins=10)
```

## Editor

To edit your code, you can navigate to the file using the **Explorer** and click on it.  This should open it in the CoCalc editor in a new tab.  If you copy the URL into a new browser tab, you can place them side by side and edit the code in one, while executing the notebook in another.

### Advanced Usage

* **JupyterLab**: CoCalc supports JupyterLab without collaboration.  You can use this if you like.
* **VS-Code**: CoCalc supports VS-Code editor in the browser.  You can use this if you like.
* **SSH**: I personally use [ssh to connect to
  CoCalc](https://doc.cocalc.com/account/ssh.html) and then edit code using Emacs on my
  computer.
  
## Reloading Code

One caveat when working in notebooks: you will probably want to edit your code.  To use
your new code you will need to tell python to reload it.  We give three ways here, each
being less convenient but more thorough.

1. Use the `autoreload` extension.  This works "by magic" but can get confused.
2. Explicitly reload the modules you need.  This always works, but you need to properly
   force reloading of all modules in the correct order.
3. Restart your kernel.  This always works, but you lose anything in memory and have to
   reload the notebook.  (Look for the menu item **Kernel/Restart Kernel**).
   
```python
# Option 1: the autoreload extension for notebooks
%load_ext autoreload
%autoreload 2
import MyCode.box_counting  # This should be reloaded if needed any time you execute this cell.
```

```python
# Option 2: explicit reload from python.
from importlib import reload
import MyCode.box_counting
reload(MyCode.box_counting)  # Explicitly reload.

# The potential issue: if MyCode.box_counting uses another piece of your code,
# then you will need to explicitly reload that code first to ensure you get
# any changes there.
```
