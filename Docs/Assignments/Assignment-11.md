---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.6
kernelspec:
  display_name: Python 3 (math-583)
  language: python
  name: math-583
---

```{code-cell}
:tags: [hide-cell]

import mmf_setup; mmf_setup.nbinit()
import os
from pathlib import Path
import logging
logging.getLogger("matplotlib").setLevel(logging.CRITICAL)
logging.getLogger('numba').setLevel(logging.CRITICAL)
logging.getLogger('OMP').setLevel(logging.CRITICAL)
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
```

(sec:Assignment11)=
# Assignment 11: Brownian Motion (Random Walks)

For details about Brownian motion and random walks, please see the following (part of my
[Standard Model course][]):

* [Renormalization Group Techniques](https://physics-581-the-standard-model.readthedocs.io/en/latest/ClassNotes/RenormalizationGroup.html)
* [Renormalizing Random Walks](https://physics-581-the-standard-model.readthedocs.io/en/latest/ClassNotes/RandomWalks.html)

The assignment asks you to consider Brownian motion where the steps $x_n$ are independent
and identically and independently distributed ([iid][]) with some probability density
function bracket ([PDF][]) $p(x)$.  The assignment asks you to consider two
cases:

1. A binary random walk with integer steps $x_{n} = \pm 1$ with probability $p$ and
   $1-p$ for $x_{n} \in \{+1, -1\}$ respectively.  I.e., choosing the steps by 
   flipping a coin (biased if $p \neq 0.5$):
   \begin{gather*}
     p(x) = p \delta(x-1) + (1-p)\delta(x+1). 
   \end{gather*}
2. A gaussian random walk with normally distributed steps of mean $\mu$ and standard
   deviation $\sigma$:
   \begin{gather*}
     p(x) = \frac{1}{\sqrt{2\pi}\sigma}\exp\left(\frac{-(x-\mu)^2}{2\sigma^2}\right).
   \end{gather*}

Consider the trajectory $\vect{X}$ where the particle is at position $X_{N}$ after $N$ steps:
\begin{gather*}
  X_{n} = \sum_{n=1}^{N}x_n. 
\end{gather*}

1. What is the [PDF][] $p_N(X)$ for the position after $N$ steps?  You are asked to
   empirically check the mean $\mu_N$ and standard deviation $\sigma_N$ of $X_N$.
2. For what values of $\mu$ and $\sigma$ does the gaussian random walk have the same
   statistical properties as the binary random walk with probability $p$ in the limit of
   large $N$? 
3. What are the average maximum and minimum excursions after $N$ steps?  I.e.,
   take a large number of random walks of length $N$, and take the average of the
   maximum $\max_{n \leq N} X_n$ and minimum $\min_{n \leq N} X_n$.
   :::{warning}
   I do not think these are trivial to compute analytically as Schroeder
   {cite}`Schroeder:1991` mentions in footnote 6 on page 146 of Chapter 6,
   §**Counterition Runs Rampand in Random Runs**:
   
   > Surprisingly, a certain statistics professor, who shall remain nameless, was
   > not able to derive the formula for the probability of ruin for the
   > finite-duration game which he had posed as an exercise for the students at the
   > beginning of the summer semester in 1948. During the entire semester his
   > recurrent refrain was "we'll tackle that one next week." But he never did
   > tackle it.
   
   Show how $\braket{\max \vect{X}_N}$ where $\vect{X}_{N}$ are walks of length $N$ is
   related to the probability of ruin for a finite-duration game. 
   :::
4. What is the probability of return?  I.e. what is the probability that a random walk
   will return to $X_N = 0$?

[Standard Model course]: <https://physics-581-the-standard-model.readthedocs.io>
[iid]: <https://en.wikipedia.org/wiki/Independent_and_identically_distributed_random_variables>
[PDF]: <https://en.wikipedia.org/wiki/Probability_density_function>
