---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.6
kernelspec:
  display_name: Python 3 (math-583)
  language: python
  name: math-583
---

```{code-cell}
:tags: [hide-cell]

import mmf_setup; mmf_setup.nbinit()
import os
from pathlib import Path
import logging
logging.getLogger("matplotlib").setLevel(logging.CRITICAL)
logging.getLogger('numba').setLevel(logging.CRITICAL)
logging.getLogger('OMP').setLevel(logging.CRITICAL)
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
```

(sec:Assignment10)=
# Assignment 10: Fractal Dimension of Julia Sets

:::{margin}
As of the time of writing, this is not included in Wikipedia's [list of fractals by
Hausdorff dimension][].
:::
What is the fractal dimension of the Julia set $J_{N g(z)}$ corresponding to Newton's
method for finding the roots of $g(z) = z^3 - 1$?  Use your previous code examples and
assignments to estimate this with uncertainties.

**Advanced/HPC**: Color the complex plane $c\in \mathbb{C}$ with the function
$\dim_{H}(J_c)$ where $J_c$ is the standard Julia set corresponding to the iteration
$z\mapsto z^2 + c$.

:::{note}
Recall that the Julia set consists of the **boundary points**.  In the Newton case,
these are the points that are not attracted to any of the roots (i.e. that take
infinitely long to converge).  In the standard case, these points are the boundary $J_c
= \partial K_c$ of the filled-in Julia set $K_c$ which contains all points that do not
escape.)
:::


## References

[Newton's method]: <https://en.wikipedia.org/wiki/Newton's_method>
[list of fractals by Hausdorff dimension]: <https://en.wikipedia.org/wiki/List_of_fractals_by_Hausdorff_dimension>
