(sec:assignments)=
# Assignments

There are two tracks of formal assignments in this course.  Here are the regular
assignments: they will have counterparts on [CoCalc][]

```{toctree}
---
maxdepth: 1
caption: "Regular Assignments:"
glob:
---
Assignments/Assignment-*
```

```{toctree}
---
maxdepth: 1
caption: "HPC Assignments:"
glob:
---
Assignments/HPC-*
```

```{toctree}
---
maxdepth: 1
caption: "Solutions"
glob:
---
Solutions/Solution-*
```


## Computing Dimension and Measure

This is a rather open-ended assignment.  Your task is to provide solutions of varying
generality and accuracy by making appropriate assumptions.  One approach is to
significantly restrict the problem through an appropriate set of axioms, and then solve
that restricted problem.  Another approach is to try to creatively implement a solution,
and then determine after-the-fact what sorts of problems your algorithm works well for.

As you learn more about the course, you should be able to improve your solution (either
incrementally or through significant algorithmic improvements) to make it more general,
or more accurate.

:::{margin}
Part of your freedom is to choose which dimension (i.e. Hausdorff? Box? etc.), measure
etc. to compute.  Start with the easiest, then generalize.
:::
:::::{admonition} Problem Statement: Measure and Dimension

Consider a set $K$ of dimension $d$ and volume (measure) $V = H^{d}(K)$.  Given a subset
$E \subset K$, write a program that computes the following from a set of $N$ sample
points sampled from (or nearby) $E$ (i.e. the samples might have some errors):

1. The dimension $\tilde{d}$ of the set $E$,
2. the volume $\tilde{V} = H^{\tilde{d}}(E)$ of the set $E$, and 
3. the set $E$.

Of course, in most cases, these will be approximations since the samples will generally
be incomplete.  Provide justified error estimates for your results, clearly expressing
your assumptions.
:::::

## Iterated Function Systems

To solve this problem, you will need to generate samples from interesting sets $E$.  One
general class of interesting examples are provided by [iterated function systems][]
(IFSs).  See for example Chapter 9 of {cite}`Falconer:2014`.

:::::{admonition} Problem Statement: IFSs

Write a program to generate samples from the attractor (invariant set) $F$ of some IFS.
For example, you might consider the {ref}`sec:sierpinski_gasket` from Chapter 1:
{cite}`Schroeder:1991`.
:::::



## Julia Sets

Write a program to generate points in the quadratic Julia sets $J_c$.


## L-Systems

Write a program to generate and draw [L-systems][].  Reproduce figures 12 and 14 from
Chapter 11 of {cite}`Schroeder:1991`.

[L-system]: <https://en.wikipedia.org/wiki/L-system>
[CoCalc: <https://cocalc.com/projects/0ac282a7-6965-436a-bf84-157dd1f7af9f>
