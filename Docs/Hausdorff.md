# Hausdorff Measure

The Hausdorff measure of a set is
\begin{gather*}
  \mathcal{H}^{s}(E) = \lim_{\delta\rightarrow 0}\mathcal{H}^{s}_{\delta}(E)\\
  
\end{gather*}

\begin{gather*}
  w(s) = \frac{\pi^{s/2}}{\Gamma\Bigl(\frac{s}{2}+1\Bigr}
\end{gather*}


## Special Cases

{cite}`Schroeder:1991` discusses several cases when the Hausdorff dimension differs from
other dimensions.

* p. 41: **Could Minkowski Hear the Shape of a Drum?**
  Michael Berry made a conjecture about scaling with non-smooth drum boundaries being
  governed by the Hausdorff dimension $D_H$, but this turned out to be incorrect: the
  Minkowski dimension $D_M$ is needed instead.
  
* Kevin also mentioned the set of rational numbers $\mathcal{Q}$.
