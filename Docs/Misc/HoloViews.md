---
jupytext:
  formats: ipynb,md:myst,py:light
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (math-583)
  language: python
  name: math-583
---

# HoloViews

[HoloViews][] is a plotting library that aids in composing and connecting plots for
interactive investigation of data.  Here we discuss how to use it to make a simple
explorer for the Mandelbrot and Julia sets.  To start, there are some examples, but
these don't do exactly what I want, so this page documents how to move beyond these.

* [Mandelbrot](https://holoviews.org/gallery/apps/bokeh/mandelbrot.html): Bokeh app that
  allows for zooming into the Mandelbrot set.  The key features here are:
  * Fast generation of data using [Numba][].
  * {class}`holoviews.Image <holoviews.element.Image>`:
    *(E.g. {doc}`holoviews:reference/elements/bokeh/Image.html`.)*
    Represents a 2D image, in this case the escape-times for the Mandelbrot set.
  * {class}`holoviews.streams.RangeXY`: 
    *(E.g. {doc}`holoviews:reference/streams/bokeh/RangeXY`.)*
    A **stream** is a source of information that changes over time.  In this case, the
    stream represents a range (box) in the x-y plane representing the region we want to
    view.  This stream connects the user's panning and zooming to the generation of the image.
  * {class}`holoviews.DynamicMap <holoviews.core.DynamicMap>`: 
    *(E.g. {doc}`holoviews:reference/containers/bokeh/DynamicMap` with [Bokeh][].)*
    The data displayed in the `Image` must be generated as needed in response to the
    user's choice of `RangeXY` as they pan and zoom.  This encapsulates a **map** (think
    dictionary) from the input range to the output image.  It is a specialization of the
    {class}`holoviews.HoloMap <holoviews.core.HoloMap>` container which represents a
    concrete dictionary. Unlike the `HoloMap`, the data for a `DynamicMap` is generated
    on the fly, and so the complete rendering will not work in a static site, and
    requires a running kernel (more on this later).
* [Verhulst Mandelbrot](
    https://holoviews.org/gallery/demos/bokeh/verhulst_mandelbrot.html): An
  overlay of the Mandelbrot set with the Verhulst bifurcation diagram.
  
```{code-cell}
:tags: [hide-input]
from itertools import islice
import numpy as np
import holoviews as hv
from holoviews import opts
hv.extension('bokeh')
hv.opts.defaults(hv.opts.Image(active_tools=['pan']))

# Area of the complex plane
bounds = (-2,-1.4,0.8,1.4)
# Growth rates used in the logistic map
growth_rates = np.linspace(0.9, 4, 1000)
# Bifurcation points
bifurcations = [1, 3, 3.4494, 3.5440, 3.5644, 3.7381, 3.7510, 3.8284, 3.8481]


def mandelbrot_generator(h,w, maxit, bounds=bounds):
    "Generator that yields the mandlebrot set."
    (l,b,r,t) = bounds
    y,x = np.ogrid[b:t : h*1j, l:r:w*1j]
    c = x+y*1j
    z = c
    divtime = maxit + np.zeros(z.shape, dtype=int)
    for i in range(maxit):
        z  = z**2 + c
        diverge = z*np.conj(z) > 2**2
        div_now = diverge & (divtime==maxit)
        divtime[div_now] = i
        z[diverge] = 2
        yield divtime
        
def mandelbrot(h,w, n, maxit):
    "Returns the mandelbrot set computed to maxit"
    iterable =  mandelbrot_generator(h,w, maxit)
    return next(islice(iterable, n, None))

def mapping(r):
    "Linear mapping applied to the logistic bifurcation diagram"
    return (r /2.0) * ( 1 - (r/2.0))

def logistic_map(gens=20, init=0.5, growth=0.5):
    population = [init]
    for gen in range(gens-1):
        current = population[gen]
        population.append(current * growth * (1 - current))
    return population
    
bifurcation_diagram = hv.Points([(mapping(rate), pop) for rate in growth_rates for 
             (gen, pop) in enumerate(logistic_map(gens=110, growth=rate))
             if gen>=100])  # Discard the first 100 generations to view attractors more easily

vlines = hv.Overlay([hv.Curve([(mapping(pos),0), ((mapping(pos),1.4))]) for pos in bifurcations])
overlay = (hv.Image(mandelbrot(800,800, 45, 46).copy(), bounds=(-2, -1.4, 0.8, 1.4))
           * bifurcation_diagram * hv.HLine(0) * vlines)

hv.output(size=150)
overlay.opts(
    opts.HLine(color='k', line_dash='dashed'),
    opts.Image(cmap='Reds', logz=True, xaxis=None, yaxis=None),
    opts.Points(size=0.5, color='g'),
    opts.Curve(color='teal', line_width=1))
```
  

## User Experience (UX)

:::{margin}
Note that these particular options, like the `wheel_scroll` tool, are specific to the
[Bokeh][] extension.  One of the issues I have with [HoloViews][] is that there are
really a lot of incompatibilities with different extensions, so your code is not easily
portable from one extension to another.  (Perhaps there is a way to write portable
code... I would like to see it.)
:::
One thing that is annoying with the examples is that `wheel_scroll` is enabled by
default.  This causes a quasi-random behavior that if one scrolls through the page,
accidentally landing on a plot, then the scrolling is hijacked and turned into zooming
on that plot.  It turns out that the tools can be set by changing the options:

```python
import holoviews as hv
hv.extension('bokeh')
hv.opts.defaults(hv.opts.Image(active_tools=['pan']))
```

These things are discussed somewhat in {doc}`holoviews:getting_started/Customization`
and {doc}`holoviews:user_guide/Applying_Customizations`, and managed by
{class}`holoviews.util.opts`, but, despite a specific section [Discovering
Options](https://holoviews.org/getting_started/Customization.html#discovering-options),
I have not yet found a good way to actually discover what the options are (especially
what the default values are).

https://holoviews.org/getting_started/Customization.html
* [Discourse: Best wat to turn off zoom tool by
  default](https://discourse.holoviz.org/t/best-way-to-turn-off-zoom-tool-by-default/5590):
  Suggests `plot.opts(default_tools=["pan"])`.
  


[HoloViews]: <https://holoviews.org>
[Numba]: <https://numba.readthedocs.io>
[Bokeh]: <https://bokeh.org/>
