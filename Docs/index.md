<!-- Math 583 - Analysis of and on Rough Sets
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
-->


<!-- Include ../README.md
     If you would like to use the contents of your top-level README.md file here, then
     you can literally include it here with the following:

```{include} ../README.md
``` 

    Note that this may will break `sphinx-autobuild` (`make doc-server`) which will not rebuild
    this index file when ../README.md changes.  See the note at the bottom of the file
    if you want to do this while using sphinx-autobuild.
--> 

```{toctree}
---
maxdepth: 2
titlesonly:
hidden:
---
Syllabus
Assignments
References
```

# Welcome

Welcome to Math 583 - Analysis of and on Rough Sets!  This is the main documentation page for the
course.  For more class information, please see the {ref}`sec:sylabus`.

Currently, this course documentation is a collection of topics listed on the left,
mostly dipping into {cite}`Schroeder:1991`.  As a starting point, you might want to try
doing the {ref}`sec:Assignments`.  The following topics are also a good starting point:

The following is one (incomplete) path through the various documents
```{toctree}
---
maxdepth: 2
caption: "A Path Forward"
---
FractalsChaosPowerLaws/GamblersRuin
FractalsChaosPowerLaws/CantorSet
FractalsChaosPowerLaws/Lsystems
FractalsChaosPowerLaws/Julia
FractalsChaosPowerLaws/Mandelbrot
FractalsChaosPowerLaws/LogisticMap
FractalsChaosPowerLaws/IteratedFunctionSystems
```

Additional details about Brownian motion and random walks can be found as part of my
Standard Model course:

* [Renormalization Group Techniques](https://physics-581-the-standard-model.readthedocs.io/en/latest/ClassNotes/RenormalizationGroup.html)
* [Renormalizing Random Walks](https://physics-581-the-standard-model.readthedocs.io/en/latest/ClassNotes/RandomWalks.html)

<!-- Instructors: the information presented here at the start of the course documentation is -->
<!-- contained in the `Docs/index.md` file, which you should edit to provide an overview of -->
<!-- the course. -->

<!-- One reasonable option might be to replace this by a literal include of the top-level -->
<!-- `README.md` file with the following code: -->

<!-- ````markdown -->
<!-- ```{include} ../README.md -->
<!-- ```  -->
<!-- ```` -->

```{toctree}
---
maxdepth: 2
caption: "Prerequisites:"
titlesonly:
hidden:
glob:
---
Prerequisites/*
```

```{toctree}
---
maxdepth: 2
caption: "Fractals, Chaos, and Power Laws:"
titlesonly:
hidden:
glob:
---
FractalsChaosPowerLaws/*
```


<!-- ```{toctree} -->
<!-- --- -->
<!-- maxdepth: 2 -->
<!-- caption: "Miscellaneous:" -->
<!-- hidden: -->
<!-- --- -->
<!-- Demonstration -->
<!-- CoCalc -->
<!-- ClassLog -->
<!-- ../InstructorNotes -->

<!-- README.md <../README> -->
<!-- ``` -->

<!-- If you opt to literally include files like ../README.md and would like to be able
     to take advantage of `sphinx-autobuild` (`make doc-server`), then you must make
     sure that you pass the name of any of these files to `sphinx-autobuild` in the
     `Makefile` so that those files will be regenerated.  We do this already for
     `index.md` but leave this note in case you want to do this elsewhere.
     
     Alternatively, you can include them separately and view these directly when editing.
     We do not include this extra toc when we build on RTD or on CoCalc.  We do this
     using the `sphinx.ext.ifconfig extension`:
     
     https://www.sphinx-doc.org/en/master/usage/extensions/ifconfig.html

```{eval-rst}
.. ifconfig:: not on_rtd and not on_cocalc

   .. toctree::
      :maxdepth: 0
      :caption: Top-level Files:
      :titlesonly:
      :hidden:

      README.md <../README>
      InstructorNotes.md <../InstructorNotes>
```
-->
